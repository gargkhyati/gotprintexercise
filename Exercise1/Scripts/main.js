$(document).ready(function() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var users = JSON.parse(this.responseText);
        showUsers(users)
      }
    };
    xhttp.open("GET", "https://jsonplaceholder.typicode.com/users", true);
    xhttp.send();

    function showUsers(users) {
      users = users.filter(function(user) {
        return parseInt(user.id, 10) % 2 == 0
      })
      function compare(a,b) {
        var aLast = (a.name.split(" ")).pop()
        var bLast = (b.name.split(" ")).pop()
        if (aLast < bLast)
          return -1;
        if (aLast > bLast)
          return 1;
        return 0;
      }
      users.sort(compare);
      var userContainer = document.getElementById('userContainer')
      users.forEach(function(user) {
        createUserView(user, userContainer)
      })

    }
    function createUserView(user, userContainer) {
      var templateData = ['id', 'name', 'username', 'street', 'suite', 'city', 'zipcode',
       'phone', 'website', 'company'];
       var mainDiv = document.createElement("div");
       mainDiv.className = "user";
       templateData.forEach(function(key) {
         var para = document.createElement('p')
         var spanLabel = document.createElement('span')
         spanLabel.className = "label"
         var spanValue = document.createElement('span')
         spanValue.className = "value"
         para.appendChild(spanLabel)
         para.appendChild(spanValue)
         spanLabel.textContent = key+" : "

         switch (key) {
           case 'id':
           case 'name':
           case 'username':
           case 'phone':
           case 'website':
             spanValue.textContent = user[key];
             break;
           case 'company':
             spanValue.textContent = user.company.name;
             break
           default:
             spanValue.textContent = user.address[key];

         }
         mainDiv.appendChild(para)

       })
       var gmapDiv = document.createElement("div")
       gmapDiv.id = "gmapDiv"+user.id
       gmapDiv.className = "gmapDiv";
       mainDiv.appendChild(gmapDiv)
       userContainer.appendChild(mainDiv)
       var map = new GMaps({
         div: "#"+gmapDiv.id,
         lat: user.address.geo.lat,
         lng: user.address.geo.lng
       });
       map.removeMarkers();
       map.addMarker({
         lat: user.address.geo.lat,
         lng: user.address.geo.lng,

      });
    }

})
