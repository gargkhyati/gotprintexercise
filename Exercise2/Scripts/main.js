$(document).ready(function() {

  var canvasCount = Math.floor(Math.random()*4 + 2)
  var canvasContainer = document.getElementById('canvasContainer')
  var shapesDrpDwn = document.getElementById('shapes')
  var canvasesDrpDwn = document.getElementById('canvases')

  var shapeMap = {
    Circle: fabric.Circle,
    Ellipse: fabric.Ellipse,
    Line: fabric.Line,
    Polygon: fabric.Polygon,
    Polyline: fabric.Polyline,
    Rectangle: fabric.Rect,
    Triangle: fabric.Triangle
  }
  Object.freeze(shapeMap);
  var fabricCanvases = [];
  var activeObject, initialCanvas;
    function createView() {
      for (var i = 0; i < canvasCount; i++) {
        var canvasEle = document.createElement("canvas");
        canvasEle.id = "canvas_"+(i+1);
        canvasEle.className = "canvas"
        canvasContainer.appendChild(canvasEle)
        var fabricObj = new fabric.Canvas(canvasEle.id, {})
        fabricCanvases.push(fabricObj)
        fabricObj.on('mouse:down', function() {
          if(this.getActiveObject()) {
            activeObject  = $.extend({}, this.getActiveObject());
            initialCanvas = this.lowerCanvasEl.id;
            console.log(initialCanvas);
          }
        });

        document.onmouseup = function(evt){
            if(evt.srcElement.tagName === 'CANVAS' && initialCanvas) {
              canvasId = evt.target.offsetParent.firstChild.id;
              var newInd = parseInt(canvasId.split("_").pop(), 10)-1
              if(canvasId !== initialCanvas) {
                fabricCanvases[newInd].add(activeObject);
                fabricCanvases[newInd].renderAll();
                // these two lines will make the original object snap back to whatever position you define
                var intId = parseInt(initialCanvas.split("_").pop(), 10)-1;
                fabricCanvases[intId].getActiveObject().remove();
                //  fabricCanvases[intId].getActiveObject().setLeft(0);
              }
            }
            initialCanvas = '';
            activeObject  = {};
        }

        var option = document.createElement("option");
        option.value = "canvas_"+i;
        option.textContent = "Canvas "+(i+1);
        canvasesDrpDwn.appendChild(option);
      }
      for (var shape in shapeMap) {
        var option = document.createElement("option");
        option.value = shape;
        option.textContent = shape;
        shapesDrpDwn.appendChild(option);
      }
      document.getElementById('addShape').onclick = addShape;
    }

    createView()
    function random_num(value) {
      var result = Math.floor(Math.random() * value);
      return result;
    }

    function addShape() {
      var canvasObj = fabricCanvases[canvasesDrpDwn.value.split("_").pop()];
      var method = shapeMap[shapesDrpDwn.value]
      var random = Math.floor(Math.random()*5 + 1)  // To get a random value for sizing/placing shapes
      var data={}
      switch (shapesDrpDwn.value) {
        case 'Circle':
          data = {radius: canvasObj.width/(5*random), fill: 'rgb(' + random_num(255) + ',' + random_num(255) + ',' + random_num(255) + ')', left: 0, top: 0}
          break;
        case 'Ellipse':
          data = {left: canvasObj.width/(5*random), top: canvasObj.height/(5*random), strokeWidth: 1, stroke: 'black', fill: 'transparent', selectable: true, originX: 'center', originY: 'center', rx: canvasObj.width/(5*random), ry: canvasObj.height/(5*random)}
          break;
        case 'Line':
          data = [canvasObj.width/(4*random), canvasObj.height/(4*random), canvasObj.width/(random), canvasObj.height/(random)];
           break;
        case 'Polygon':
          data = [{x: 200, y: 0},{x: 250, y: 50},{x: 250, y: 100},{x: 150, y: 100},{x: 150, y: 50} ];
          break
        case 'Polyline':
          var line = [[100,0], [100,100], [150,100], [200,100], [150,50], [100,450]];
          var data = [];
          line.forEach(function(i){
            data.push({x: i[0], y: i[1]});
          });

          break;
        case 'Rectangle':
          data = {width: canvasObj.width/(2*random), height: canvasObj.height/(2*random), fill: 'rgb(' + random_num(255) + ',' + random_num(255) + ',' + random_num(255) + ')', opacity: 1, left: 0, top: 0}
          break;
        case 'Triangle':
        data = {left: random*20, top: 10*random, strokeWidth: 1, width: canvasObj.width/(2*random), height: canvasObj.height/(2*random), stroke: 'black', fill:'rgb(' + random_num(255) + ',' + random_num(255) + ',' + random_num(255) + ')', selectable: true}
        default:

      }

      if (shapesDrpDwn.value == 'Line' || shapesDrpDwn.value == 'Polygon'|| shapesDrpDwn.value == 'Polyline') {
        canvasObj.add(new method(data, {
        stroke: 'rgb(' + random_num(255) + ',' + random_num(255) + ',' + random_num(255) + ')',
        fill:"transparent",
        left:random_num(canvasObj.width/2),
        top: 50
      }))
    } else {
        canvasObj.add(new method(data))
      }


      return false
    }
})
